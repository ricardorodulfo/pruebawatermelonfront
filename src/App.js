import React from 'react';
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";
import Registro from './Registro';
import IniciarSesion from './IniciarSesion';
import Home from './Home';

function App() {
    return (
        <BrowserRouter>
            <Navbar bg="dark" variant="dark">
                <Nav className="mr-auto">
                    <Link to="/home">
                        <Nav.Link href="#home">Home</Nav.Link>
                    </Link>
                    <Link to="/iniciar-sesion">
                        <Nav.Link href="#features">Entrar</Nav.Link>
                    </Link>
                    <Link to="/registro">
                        <Nav.Link href="#pricing">Salir</Nav.Link>
                    </Link>
                </Nav>
            </Navbar>
            <Switch>
                <Route path="/" exact component={Home}></Route>
                <Route path="/home" exact component={Home}></Route>
                <Route path="/registro" component={Registro}></Route>
                <Route path="/iniciar-sesion" component={IniciarSesion}></Route>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
