import React from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Home extends React.Component
{
    constructor()
    {
        super();

        this.historyIniciarSesion = false;

        this.usuarioRef = React.createRef();
        this.contraseniaRef = React.createRef();
        this.loginUser = this.loginUser.bind(this);
    }

    loginUser()
    {
        let loginData = {
            usuario: this.usuarioRef.current.value,
            contrasenia: this.contraseniaRef.current.value
        };

        fetch('http://localhost:8080/login', {
            method: 'POST',
            body: JSON.stringify(loginData),
            mode: 'cors',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }})
            .then(function(response){
                return response.json();
            })
            .then(function(data){

                if(!data.error)
                {
                    this.setState({
                        historyIniciarSesion: !this.state.historyIniciarSesion
                    })
                }
                
            }.bind(this));
    }

    render()
    {
        if(this.historyIniciarSesion)
        {
            return <Redirect to='/iniciar-sesion' />
        }

        return (
            <Container>
                <Row className="justify-content-md-center">
                    <Col md={4} offset={4}>
                        <Form onSubmit={this.loginUser}>
                            <Form.Label><h4>Ingreso</h4></Form.Label>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Ingrese Email" 
                                    ref={this.usuarioRef}/>
                                <Form.Text className="text-muted">
                                    Nunca compartiremos tu email con nadie.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Contrasenia</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Contrasenia" 
                                    ref={this.contraseniaRef}/>
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Ingresar
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}