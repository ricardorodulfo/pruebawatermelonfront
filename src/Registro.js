import React from 'react';
import { withRouter, Redirect } from "react-router-dom";
import { Button, Container, Row, Col, Form } from 'react-bootstrap';


class Registro extends React.Component 
{

    constructor(props)
    {
        super(props);

        this.state = {
            historyHome: false
        };

        this.usuarioRef = React.createRef();
        this.contraseniaRef = React.createRef();
        this.registerUser = this.registerUser.bind(this);   
    }

    registerUser(e)
    {
        e.preventDefault();
        let userData = {
            usuario: this.usuarioRef.current.value,
            contrasenia: this.contraseniaRef.current.value
        };

        fetch('http://localhost:8080/register', { 
            method: 'POST', 
            body: JSON.stringify(userData),
            mode: 'cors',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            }})
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                this.setState({
                    historyHome: !this.state.historyHome
                });
            }.bind(this));
    }

    onCuerpo()
    {
        this.props.history.push("/iniciar-sesion");
    }

    render()
    {
        if(this.state.historyHome)
        {
            return <Redirect to='/home' />
        }

        return (
            <Container>
                <Row className="justify-content-md-center">
                    <Col md={4} offset={4}>
                        <Form onSubmit={this.registerUser}>
                            <Form.Label><h4>Registro</h4></Form.Label>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Ingrese Email" 
                                    ref={this.usuarioRef}/>
                                <Form.Text className="text-muted">
                                    Nunca compartiremos tu email con nadie.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Contrasenia</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Contrasenia" 
                                    ref={this.contraseniaRef}/>
                            </Form.Group>
                            
                            <Button variant="primary" type="submit">
                                Registrar
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default withRouter(Registro);